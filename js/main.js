$(document).ready(function(){
  
  
//  header buton
  
  $('span.dropdown').click(function(e){
    $(this).children('.dropdown-menu').slideToggle(200);
    e.stopPropagation();
  });
  $('html body').click(function(){
    $('header .header-right .dropdown-menu').slideUp(100);
  });
  
//  navbar
  
  $('nav .nav-btn').click(function(){
    
    $('nav .nav-menu').css('left',0);
  });
  
  $('nav .nav-menu a').click(function(){
    $(this).closest('.nav-menu').css('left','100%');
  });
  
  
//  close search panel 
  $('header .header-right span.fa-search').click(function(){
    $('.search-panel').addClass('active');
  });
  $('.search-panel-close').click(function(){
    $('.search-panel').removeClass("active");
  });
  
  
//  modal
  $('.modal .modal-panel-close').click(function(){
    $('.modal').removeClass('active');
  });
  
  $('nav span.modal-btn').click(function(){
    $('.modal').addClass('active')
  });
  
  $('.modal').click(function(){
    
    $('.modal-panel').click(function(e){
      e.stopPropagation();
    });
    
    $('.modal').removeClass('active');
  });
  
  $('.modal .modal-panel span.fa-search').click(function(e){
    $('.search-panel').addClass('active');
    e.stopPropagation();
  });
  
  
  // main content slider
  var $sliderWrapper = $('.main-wrapper');
  var $slider = $('ul.slides');
  
  var next = 1;
  var current = 1;
  
  $sliderWrapper.height($slider.children('li:nth-child(1)').outerHeight());
  
  $(window).resize(function(){
    $sliderWrapper.height($slider.children('li:nth-child('+current+')').outerHeight());
  });
  
  function showNext(a){
    if (a > 8){ a = 1 }
    if (a < 1){ a = 8 }
    var n = (a-1) * 12.5;
    $slider.css("transform","translateX(-"+n+"%)");
    $sliderWrapper.height($slider.children('li:nth-child('+a+')').outerHeight());
    current = a;
    $('nav .nav-menu li.active').removeClass('active');
    $("nav .nav-menu li[data-slide="+a+"]").addClass('active');
  }
  
  $('nav .nav-menu li').click(function(){
    
    var n = $(this).attr('data-slide');
    showNext(n);
  });
  
  
  $('.page-btn .prevpage').click(function(){
    showNext(current - 1);
  });
  $('.page-btn .nextpage').click(function(){
    showNext(current + 1);
  });
//  setInterval(function(){
//    
//    if (n > 8){ n = 1 }
//    showNext(n);
//    n = n + 1;
//  }, 1500);
  
  
  
//  experience carousel
  var $carousel =  $('.experience-page > .carousel > .carousel-item');
  var $indicator = $('.experience-page > .carousel > .carousel-indicator');
  var currentEx = 1;
  function nextCarousel(a){
    if (a > 3){ a = 1 }
    if (a < 1){ a = 3 }
    
    var n = (a-1) * 33.33;
    $carousel.css('transform','translateX(-'+n+'%)');
    
    $indicator.children('li.active').removeClass('active');
    $indicator.children('li:nth-child('+a+')').addClass('active');
    currentEx = a;
  }
  
  $indicator.children('li').click(function(){
    var next = $(this).attr('data-carousel');
    nextCarousel(next);
  });
  
  setInterval(function(){
    nextCarousel(currentEx + 1);
  },3000);
  
  

  //blog masonry
  $('.blog-wrapper').masonry({
    itemSelector: ".blog-item",
    columnWidth: 290
    
  });
  
//  product work-page
  var mixer = mixitup('.work-product');
});